import { Injectable } from '@angular/core';
import {AuthService} from './auth.service'; // הוספת אימפורט עם נקודה אחת לפני כי חוזרים תיקייה אחת אחורה ולא שתיים
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה


@Injectable({
  providedIn: 'root'
})
export class TodosService {

  delete(key){ // הוספת פונקציית מחיקה
    console.log ("workkkkkk");
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos').remove(key);
    })
  }


  update(key,text){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text});
    })
  }

  addTodo(text:string){ // הפונקציה מקבלת מהקומפוננט טקסט איזה טודו להוסיף
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').push({'text':text});
    })
  }
  constructor(private authService:AuthService,
              private db:AngularFireDatabase,
            ) { }
}
